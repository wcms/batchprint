<?php
/**
 * Created by PhpStorm.
 * User: wolf
 * Date: 16/2/18
 * Time: 上午10:34
 */
header("Content-type: text/html; charset=GBK");
error_reporting(E_ALL^E_NOTICE);
$csvSer=new CSVService();
$csvSer->getFile();
$days=$csvSer->getDate();
$con=$csvSer->run();
$depart=$csvSer->getDepart();
$sno=$csvSer->getSNO();
$total=$csvSer->getTotal();
class CSVService{

    private $_mincate=4;
    private $_maxcate=9;
    private $_randNum;


    private $_cateArr;//存储产品品类 有相同的
    private $_data1;//不重复的产品
    private $_days='5,15,25';
    private $_data2;//重复2遍产品
    private $_data3;//重复3遍产品
    private $_debug=0;
    private $_depeart;
    private $_sno;

    private $_total=0;


    public function __construct(){

        $this->_days=explode(",",trim($_POST['days']));
        $this->_year=$_POST['year'];
        $this->_debug=$_POST['debug'];
        $this->_depeart=$_POST['depart'];
        $this->_sno=$_POST['sno'];
    }

    public function getDepart(){
        return $this->_depeart;
    }

    public function getSNO(){
        return $this->_sno;
    }


    public function getDate(){

        $day=array();
        foreach($this->_days as $v){

            $day[]=$this->_year.$v;
        }
        return $day;
    }

    public function getFile(){

        $file=$_FILES['csv']['tmp_name'];

        if(!file_exists($file)){
            echo "file is not exsits!";
            exit();
        }

        $handle=fopen($file,"r");

        $i=0;

        while($line=fgets($handle)){
            $i++;
            if($i==1){
                continue;
            }
            $line=str_replace(array("\r\n","\r","\n"),"",$line);

            $cate=explode(",",trim($line),4);

            $this->parseCate($cate);

        }

        fclose($handle);
    }



    //调试查看是否分配正确
    public function debug(){

        foreach($this->_data1 as $v){

            echo implode(",",$v);
            echo "\r\n";
        }
        foreach($this->_data2 as $v){

            echo implode(",",$v);
            echo "\r\n";
        }

        foreach($this->_data3 as $v){

            echo implode(",",$v);
            echo "\r\n";
        }

    }


    private function setCsvHeader()
    {
        header("Cache-Control: public");
        header("Pragma: public");
        header("Content-type:application/vnd.ms-excel");
        $file = date("md", time());
        header("Content-Disposition:attachment;filename=$file.csv");
    }


    public function parseCate($cate){

        if($cate[2]<10){
            $this->_data1[]=$cate;
            return;
        }
        //分成多少块
        $div=$cate[2]<100?2:3;
        $rs=$this->randomSplit($div,trim($cate[2]));
        foreach($rs as $v){
            $cate[2]=$v;

            if($div==2){
                $this->_data2[]=$cate;
            }else{
                $this->_data3[]=$cate;
            }
        }
        return;

    }

    public function getTotal(){
        return $this->_total;
    }


    private function randomSplit($n, $sum) {
        $arr= array();
        for($i = 1; $i < $n; $i++) {
            $value	= mt_rand(1, floor($sum/($n-$i+1)));
            $arr[]	= $value;
            $sum	-= $value;
        }
        $arr[]	= $sum;
        return $arr;
    }

    //一天内领取 如果数量为0  那么就删除该品类 一组数据
    public function exportData($day,$data){

        $format="<tr><td align=\"center\">%s</td><td>%s</td><td align=\"center\">%s</td><td align=\"center\">%s</td><td align=\"center\">%s</td></tr>";
        $con="";
        foreach($data as $v){

            $name=str_replace(array("\r\n","\r","\n"),"",$v[3]);
            //查看获取
            $con.= sprintf($format,$v[0],trim($name),$v[1],trim($v[2]),"领料");
            $this->_total+=trim($v[2]);
        }
        return $con;

    }



    //根据时间进行排列

    public function run(){

        if($this->_debug){
            $this->setCsvHeader();
            $this->debug();
            return;
        }

        //取3次
        $data=array();
        $con="";
        for($i=1;$i<=3;$i++){

            $con= $this->exportData($i-1,$this->getData1($i));
            $con.= $this->exportData($i-1,$this->getData2($i));
            $con.=$this->exportData($i-1,$this->getData3($i));

            $data[$i]=$con;
            $con="";
        }
        return $data;

    }


    public function getData1($round){
        if($round==3){
            return $this->_data1;
        }else{
            $data1Num=rand(1,count($this->_data1)-2);//截取长度
            $data= array_slice($this->_data1,0,$data1Num);
            array_splice($this->_data1,0,$data1Num);
            return $data;
        }
    }

    //重复2
    public function getData2($round){


        if($round==3){
            return array();
        }

        $i=1;
        $round1=array();
        $round2=array();
        foreach($this->_data2 as $k=>$v){
            if($i%2==0){
                $round1[]=$v;
            }else
            {
                $round2[]=$v;
            }
            $i++;
        }

        return $round==1?$round1:$round2;

    }

    //重复3
    public function getData3($round){

        $i=1;
        $round1=array();
        $round2=array();
        $round3=array();
        foreach($this->_data3 as $k=>$v){
            if($i%3==0){
                $round3[]=$v;
            }else if($i%2==0)
            {
                $round2[]=$v;
            }else{
                $round1[]=$v;
            }
            $i++;
        }

        switch ($round){
            case 1:
                $data= $round1;
                break;
            case 2:
                $data= $round2;
                break;
            case 3:
                $data=  $round3;
                break;
        }
        return $data;

    }



}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=GBK" />
    <title>顶上智能家居 领料单打印</title>

    <script language="javascript" src="LodopFuncs.js"></script>
    <style>

        body{width:1220px;margin:0 auto;}
        .check{color:red;}

    </style>
</head>
<body>

<p><a href="javascript:PreviewMytable();">打印预览</a>   <span class="check">合计领取数量:<?=$total?></span></p>



<p>----------------------div1:------------------------------------------------------------------------------------</p>
<div id="div1">
    <DIV style="LINE-HEIGHT: 30px" class=size16 align=center><STRONG><font color="#0000FF" style="font-size:20px;">浙江移动电气股份有限公司 材料领料单</font></STRONG></DIV>
    <TABLE border=0 cellSpacing=0 cellPadding=0 width="100%">
        <TBODY>
        <TR>
            <TD width="43%"><font color="#0000FF">领料部门：<SPAN
                        id=rpt_Pro_Order_List_ctl00_lbl_eShop_Name><?=$depart?></SPAN></font></TD>
            <TD width="33%"><font color="#0000FF">日期：<SPAN ><?= $days[0]?></SPAN></font></TD>
            <TD><font color="#0000FF">材料仓库 No:<span tdata='pageNO+<?= $sno?>'>##</span></font></TD></TR>

        </TBODY></TABLE>
</div>
<p>----------------------div2:------------------------------------------------------------------------------------</p>
<div id="div2">

    <TABLE border=1 cellSpacing=0 cellPadding=1 width="100%" style="border-collapse:collapse" bordercolor="#333333">
        <thead>
        <TR>
            <TD width="10%">
                <DIV align=center><b>材料货号</b></DIV></TD>
            <TD width="50%">
                <DIV align=center><b>材料名称</b></DIV></TD>
            <TD width="10%">
                <DIV align=center><b>单位</b></DIV></TD>
            <TD width="15%">
                <DIV align=center><b>领料数量</b></DIV></TD>

            <TD width="15%">
                <DIV align=center><b>领用类别</b></DIV></TD></TR>
        </thead>
        <TBODY>
        <?=$con[1]?>

        <tfoot>
        <tr>
            <TD ><b>合计</b></TD>
            <TD >

            </TD>
            <TD >
            </TD>
            <TD width="19%" tdata="subSum" format="#,##0.00" align="center"><font color="#0000FF">###</font></TD>

            <TD width="14%" align="right">　</TD>
        </tr>
        </tfoot>
    </TABLE>
</div>
<p>----------------------div3:------------------------------------------------------------------------------------</p>
<div id="div3">
    <DIV style="LINE-HEIGHT: 30px"
         align=left><font color="#0000FF">发料人:</font>                      <font color="#0000FF">领料人:</font></DIV>
</div>
<p>----------------------div4:------------------------------------------------------------------------------------</p>
<div id="div4">
    <DIV style="LINE-HEIGHT: 30px" class=size16 align=center><STRONG><font color="#0000FF" style="font-size:20px;">浙江移动电气股份有限公司 材料领料单</font></STRONG></DIV>
    <TABLE border=0 cellSpacing=0 cellPadding=0 width="100%">
        <TBODY>
        <TR>
            <TD width="43%"><font color="#0000FF">领料部门：<SPAN
                        id=rpt_Pro_Order_List_ctl00_lbl_eShop_Name><?=$depart?></SPAN></font></TD>
            <TD width="33%"><font color="#0000FF">日期：<SPAN ><?=$days[1]?></SPAN></font></TD>
            <TD><font color="#0000FF">材料仓库 No:<span tdata='pageNO+<?= $sno?>'>##</span></font></TD></TR>

        </TBODY></TABLE>
</div>
<p>---------------------------------------------------------------------------------------------------------------</p>

<div id="div5">

    <TABLE border=1 cellSpacing=0 cellPadding=1 width="100%" style="border-collapse:collapse" bordercolor="#333333">
        <thead>
        <TR>
            <TD width="10%">
                <DIV align=center><b>材料货号</b></DIV></TD>
            <TD width="50%">
                <DIV align=center><b>材料名称</b></DIV></TD>
            <TD width="10%">
                <DIV align=center><b>单位</b></DIV></TD>
            <TD width="15%">
                <DIV align=center><b>领料数量</b></DIV></TD>

            <TD width="15%">
                <DIV align=center><b>领用类别</b></DIV></TD></TR>
        </thead>
        <TBODY>
        <?=$con[2]?>

        <tfoot>
        <tr>
            <TD ><b>合计</b></TD>
            <TD >

            </TD>
            <TD >
                </TD>
            <TD width="19%" tdata="subSum" format="#,##0.00" align="center"><font color="#0000FF">###</font></TD>

            <TD width="14%" align="right">　</TD>
        </tr>
        </tfoot>
    </TABLE>
</div>


<p>----------------------div7:------------------------------------------------------------------------------------</p>
<div id="div6">
    <DIV style="LINE-HEIGHT: 30px" class=size16 align=center><STRONG><font color="#0000FF" style="font-size:20px;">浙江移动电气股份有限公司 材料领料单</font></STRONG></DIV>
    <TABLE border=0 cellSpacing=0 cellPadding=0 width="100%">
        <TBODY>
        <TR>
            <TD width="43%"><font color="#0000FF">领料部门：<SPAN
                        id=rpt_Pro_Order_List_ctl00_lbl_eShop_Name><?=$depart?></SPAN></font></TD>
            <TD width="33%"><font color="#0000FF">日期：<SPAN ><?=$days[2]?></SPAN></font></TD>
            <TD><font color="#0000FF">材料仓库 No:<span tdata='pageNO+<?= $sno?>'>##</span></font></TD></TR>

        </TBODY></TABLE>
</div>
<p>-------------------------------------------------------div8--------------------------------------------------------</p>

<div id="div7">

    <TABLE border=1 cellSpacing=0 cellPadding=1 width="100%" style="border-collapse:collapse" bordercolor="#333333">
        <thead>
        <TR>
            <TD width="10%">
                <DIV align=center><b>材料货号</b></DIV></TD>
            <TD width="50%">
                <DIV align=center><b>材料名称</b></DIV></TD>
            <TD width="10%">
                <DIV align=center><b>单位</b></DIV></TD>
            <TD width="15%">
                <DIV align=center><b>领料数量</b></DIV></TD>

            <TD width="15%">
                <DIV align=center><b>领用类别</b></DIV></TD></TR>
        </thead>
        <TBODY>
        <?=$con[3]?>

        <tfoot>
        <tr>
            <TD ><b>合计</b></TD>
            <TD >

            </TD>
            <TD >
            </TD>
            <TD width="19%" tdata="subSum" format="#,##0.00" align="center"><font color="#0000FF">###</font></TD>

            <TD width="14%" align="right">　</TD>
        </tr>
        </tfoot>
    </TABLE>
</div>

<script>
    function PreviewMytable(){
        var LODOP=getLodop();
        LODOP.PRINT_INIT("顶上云智能打印系统");
        LODOP. SET_PRINT_PAGESIZE (1,2100,1400,"");
        var strStyle="<style> table,td,th {border-width: 1px;border-style: solid;border-collapse: collapse;font-size:12px;}</style>"
        LODOP.ADD_PRINT_TABLE(108,"5%","90%",314,strStyle+document.getElementById("div2").innerHTML);
        LODOP.SET_PRINT_STYLEA(0,"Vorient",3);
        LODOP.ADD_PRINT_HTM(26,"5%","90%",109,document.getElementById("div1").innerHTML);
        LODOP.SET_PRINT_STYLEA(0,"ItemType",1);
        LODOP.SET_PRINT_STYLEA(0,"LinkedItem",1);
      //  LODOP.ADD_PRINT_HTM(444,"5%","90%",54,document.getElementById("div3").innerHTML);
       // LODOP.SET_PRINT_STYLEA(0,"ItemType",1);
        //LODOP.SET_PRINT_STYLEA(0,"LinkedItem",1);
        LODOP. ADD_PRINT_TEXT("120.9mm","12.0mm","200.0mm","5mm","发料人                                                          领料人");
        LODOP.SET_PRINT_STYLE(0,"FontSize",14);

        LODOP.SET_PRINT_STYLEA(0,"ItemType",1);
        LODOP. ADD_PRINT_TEXT("22mm","115mm","140mm","5mm","第一联: 存根联   第二联: 财务联  第三联: 仓库联");
        LODOP.SET_PRINT_STYLEA(0,"ItemType",1);

        LODOP.NewPageA();
        LODOP.ADD_PRINT_HTM(26,"5%","90%",109,document.getElementById("div4").innerHTML);
        LODOP.SET_PRINT_STYLEA(0,"ItemType",1);
        LODOP.ADD_PRINT_TABLE(108,"5%","90%",328,strStyle+document.getElementById("div5").innerHTML);
        LODOP.SET_PRINT_STYLEA(0,"Vorient",3);

      //  LODOP.SET_PRINT_STYLEA(0,"LinkedItem",-1);


        LODOP.NewPageA();
        LODOP.ADD_PRINT_TABLE(108,"5%","90%",328,strStyle+document.getElementById("div7").innerHTML);
        LODOP.SET_PRINT_STYLEA(0,"Vorient",3);
        LODOP.ADD_PRINT_HTM(26,"5%","90%",109,document.getElementById("div6").innerHTML);
        LODOP.SET_PRINT_STYLEA(0,"ItemType",1);
        LODOP.SET_PRINT_STYLEA(0,"LinkedItem",-1);

        LODOP.PREVIEW();
    };
</script>


</body>